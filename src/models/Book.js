// Modules
const fs = require('fs');
const path = require('path');
const uuid = require('uuid');

// Path to Books.json
const p = path.join(path.dirname(require.main.filename), 'data', 'Books.json');

module.exports = class Book {
  constructor(data) {
    const { title, author, publicationYear } = data;
    this.title = title;
    this.author = author;
    this.publicationYear = publicationYear;
    this.guid = uuid.v4();
  }

  getGuid() {
    return this.guid;
  }

  // We push a new Book to Books array and save
  save() {
    // We read the file everytime we need to modify it
    fs.readFile(p, (err, data) => {
      let Books = [];
      if (!err) {
        Books = JSON.parse(data);
      }
      Books.push(this);
      // Write the file
      fs.writeFile(p, JSON.stringify(Books), (err) => console.log(err));
      
    })
  }

  // We update data with the given one
  static update(Books) {
    fs.writeFile(p, JSON.stringify(Books), (err) => console.log(err));
  }

  // get and parse the data (async)
  static getAll(cb) {
    fs.readFile(p, (err, data) => {
      let Books = [];
      if (!err) {
        Books = JSON.parse(data);
      }
      // callback function when the data is ready
      cb(Books);
    });
  }
};
