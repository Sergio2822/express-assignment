// Modules
const express = require('express');
const BookResources = express.Router();
const { body} = require('express-validator');
// Controllers
const { BookControllers } = require('../controllers');

// All user resources
BookResources.get('/', BookControllers.getAll);
BookResources.post('/', body('title').exists({checkFalsy: true}),body('author').exists({checkFalsy: true}),body('publicationYear').exists().isInt({min:1454, max:2021}),BookControllers.createBook);
BookResources.get('/:guid', BookControllers.getByGuid);
BookResources.put('/:guid', BookControllers.updateBook);
BookResources.delete('/:guid', BookControllers.deleteBook);

module.exports = BookResources;
