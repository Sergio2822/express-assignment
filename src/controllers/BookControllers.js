// Models
const { Book } = require('../models');
const{ validationResult } = require('express-validator');


// Fecth all Books
const getAll = (req, res) => {
  Book.getAll((Books) => {
    res.send(Books);
  });
};

// Get Book by guid
const getByGuid = (req, res) => {
  const { guid } = req.params;
  // Read all Book
  Book.getAll((Books) => {
    // Filter by guid
    const book = Books.find(ent => ent.guid === guid);

    if (book) {
      res.send(book);
    } else {
      res.status(404).send({
        message: 'Ups!!! book not found.',
      });
    }
  });
};




const createBook = (req, res) => {

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  // Add new book to Books
  const { body } = req;
  // Create new instance
  const newBook = new Book(body);

  Book.getAll((Books) => {
    // Filter by guid
    const bookTitle = Books.find(ent => ent.title === body.title);
    const bookAuthor = Books.find(ent => ent.author === body.author);
    const bookPY = Books.find(ent => ent.publicationYear === body.publicationYear);

    if (bookTitle && bookAuthor && bookPY) {
      res.status(404).send({
        message: 'Ups!!! Book already registered.',
      });
    } else {
      // Save in db
      newBook.save();
      res.send({
        message: 'Book successfully created!!!',
        guid: newBook.getGuid(),
      });
    }
  });

};

// Update an existing Book
const updateBook = (req, res) => {
  const { params: { guid }, body } = req;
  // Read all Book
  Book.getAll((Books) => {
    // Filter by guid
    const book = Books.find(ent => ent.guid === guid);

    if (book) {
      Object.assign(book, body);
      Book.update(Books);
      res.send({
        message: 'book successfully updated!!!',
      });
    } else {
      res.status(404).send({
        message: 'Ups!!! Book not found.',
      });
    }
  });
};

// Delete Book from Books
const deleteBook = (req, res) => {
  const { guid } = req.params;
  // Read all Book
  Book.getAll((Books) => {
    // Filter by guid
    const bookIdx = Books.findIndex(ent => ent.guid === guid);

    if (bookIdx !== -1) {
      Books.splice(bookIdx, 1);
      Book.update(Books);
      res.send({
        message: 'Book successfully deleted!!!',
      });
    } else {
      res.status(404).send({
        message: 'Ups!!! Book not found.',
      });
    }
  });
};

module.exports = {
  getAll,
  getByGuid,
  createBook,
  updateBook,
  deleteBook,
};
